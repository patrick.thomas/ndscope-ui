from qtpy import QtCore
import numpy as np


max_lin = np.finfo(float).max
max_log = np.log10(max_lin) - 1


class IdTransform:
    def y_transform(self, y):
        return y

    def transform(self, pt):
        return pt

    def y_inverse_transform(self, y):
        return y

    def inverse_transform(self, pt):
        return pt


class LogYTransform:
    def y_transform(self, y):
        if np.isfinite(y):
            if y > 0:
                return np.log10(y)
            else:
                return np.nan
        else:
            return y

    def transform(self, pt):
        x = pt.x()
        y = pt.y()
        y = self.y_transform(y)
        return QtCore.QPointF(x, y)

    def y_inverse_transform(self, y):
        if np.isfinite(y):
            if y < max_log:
                return 10 ** y
            else:
                return np.inf
        else:
            return y

    def inverse_transform(self, pt):
        x = pt.x()
        y = pt.y()
        y = self.y_inverse_transform(y)
        return QtCore.QPointF(x, y)
