from qtpy import QtCore
import pyqtgraph as pg
import numpy as np

import TransformableLine


class SingleCursor(QtCore.QObject):
    val_signal = QtCore.Signal(QtCore.QObject, QtCore.QPointF)

    def __init__(self, is_vertical, pen):
        super().__init__()

        self.is_vertical = is_vertical

        if is_vertical:
            angle = 90
            label_opts = {'position': 0, 'anchors': [(0, 1), (1, 1)]}
        else:
            angle = 0
            label_opts = {'position': 0, 'anchors': [(0, 0), (0, 1)]}

        self.line = TransformableLine.TransformableLine(
            angle=angle,
            pen=pen,
            movable=True,
            label='',
            labelOpts=label_opts
        )

        self.plot = None

        self.enabled = False

        self.on_plot_req = False
        self.on_plot = False

        self.val_signal_blocked = False

        self.line.pos_changed_signal.connect(self.pos_changed_slot)

        self.point_to_label_function = None

    def set_point_to_label_function(self, function):
        self.point_to_label_function = function

    def set_plot(self, plot):
        self.plot = plot

    def set_enabled(self, enabled):
        self.enabled = enabled
        self.update_on_plot()

    def set_transform(self, transform):
        self.line.set_transform(transform)

        # Sets the position from the value.
        val = self.line.get_val()
        pos = self.line.get_pos_for_val(val)
        if np.isfinite(pos.x()) and np.isfinite(pos.y()):
            self.line.block_pos_changed_signal(True)
            self.line.set_pos(pos)
            self.line.block_pos_changed_signal(False)

        # Sets the visibility.
        if np.isfinite(pos.x()) and np.isfinite(pos.y()):
            self.on_plot_req = True
        else:
            self.on_plot_req = False
        self.update_on_plot()

        # This does not appear correctly if it is set before the visibility.
        self.set_label_text_from_val(val)

        if not self.val_signal_blocked:
            self.val_signal.emit(self.plot, val)

    def pos_changed_slot(self, pos):
        # Sets the value from the position.
        val = self.line.get_val_for_pos(pos)
        self.line.set_val_if_finite(val)
        self.set_label_text_from_val(val)

        if not self.val_signal_blocked:
            self.val_signal.emit(self.plot, val)

    def set_val(self, val):
        # Sets the value.
        self.line.set_val_if_finite(val)

        # Sets the position from the value.
        pos = self.line.get_pos_for_val(val)
        if np.isfinite(pos.x()) and np.isfinite(pos.y()):
            self.line.block_pos_changed_signal(True)
            self.line.set_pos(pos)
            self.line.block_pos_changed_signal(False)

        # Sets the visibility.
        if np.isfinite(pos.x()) and np.isfinite(pos.y()):
            self.on_plot_req = True
        else:
            self.on_plot_req = False
        self.update_on_plot()

        # This does not appear correctly if it is set before the visibility.
        self.set_label_text_from_val(val)

        if not self.val_signal_blocked:
            self.val_signal.emit(self.plot, val)

    def set_label_text_from_val(self, val):
        if self.point_to_label_function is not None:
            label_text = self.point_to_label_function(val)
            self.line.set_label_text(label_text)

    def update_on_plot(self):
        if self.plot is not None:
            if self.enabled:
                if self.on_plot_req and not self.on_plot:
                    self.plot.addItem(self.line, ignoreBounds=True)
                    self.on_plot = True

                if not self.on_plot_req and self.on_plot:
                    self.plot.removeItem(self.line)
                    self.on_plot = False
            else:
                if self.on_plot:
                    self.plot.removeItem(self.line)
                    self.on_plot = False

    def block_val_signal(self, block):
        self.val_signal_blocked = block

    def set_foreground_color(self, color):
        self.line.pen.setColor(color)
        self.line.label.setColor(color)

    def set_font(self, font):
        self.line.label.setFont(font)


class DualCursors(QtCore.QObject):
    cursor_1_val_signal = QtCore.Signal(QtCore.QObject, QtCore.QPointF)
    cursor_2_val_signal = QtCore.Signal(QtCore.QObject, QtCore.QPointF)

    def __init__(self, is_vertical, pen):
        super().__init__()

        self.is_vertical = is_vertical

        self.cursor_1 = SingleCursor(is_vertical, pen)
        self.cursor_2 = SingleCursor(is_vertical, pen)

        if is_vertical:
            angle = 90
            label_opts = {'position': 1, 'anchors': [(0.5, 0), (0.5, 0)]}
        else:
            angle = 0
            label_opts = {'position': 1, 'anchors': [(1, 0.5), (1, 0.5)]}

        self.diff_line = TransformableLine.TransformableLine(
            angle=angle,
            pen=pg.mkPen(None),
            label='',
            labelOpts=label_opts
        )

        self.plot = None

        self.enabled = False

        self.diff_on_plot_req = False
        self.diff_on_plot = False

        self.cursor_1_val_signal_blocked = False
        self.cursor_2_val_signal_blocked = False

        self.cursor_1.val_signal.connect(self.cursor_1_val_slot)
        self.cursor_2.val_signal.connect(self.cursor_2_val_slot)

        self.diff_point_pair_to_label_function = None

    def set_cursor_1_point_to_label_function(self, function):
        self.cursor_1.set_point_to_label_function(function)

    def set_cursor_2_point_to_label_function(self, function):
        self.cursor_2.set_point_to_label_function(function)

    def set_diff_point_pair_to_label_function(self, function):
        self.diff_point_pair_to_label_function = function

    def set_plot(self, plot):
        self.cursor_1.set_plot(plot)
        self.cursor_2.set_plot(plot)
        self.plot = plot

    def set_enabled(self, enabled):
        self.cursor_1.set_enabled(enabled)
        self.cursor_2.set_enabled(enabled)
        self.enabled = enabled
        self.update_on_plot()

    def set_transform(self, transform):
        self.cursor_1.set_transform(transform)
        self.cursor_2.set_transform(transform)
        self.diff_line.set_transform(transform)
        self.update_diff_line()

    def set_cursor_1_val(self, val):
        self.cursor_1.block_val_signal(True)
        self.cursor_1.set_val(val)
        self.cursor_1.block_val_signal(False)
        self.update_diff_line()
        if not self.cursor_1_val_signal_blocked:
            self.cursor_1_val_signal.emit(self.plot, val)

    def set_cursor_2_val(self, val):
        self.cursor_2.block_val_signal(True)
        self.cursor_2.set_val(val)
        self.cursor_2.block_val_signal(False)
        self.update_diff_line()
        if not self.cursor_2_val_signal_blocked:
            self.cursor_2_val_signal.emit(self.plot, val)

    def cursor_1_val_slot(self, plot, val):
        self.update_diff_line()
        if not self.cursor_1_val_signal_blocked:
            self.cursor_1_val_signal.emit(plot, val)

    def cursor_2_val_slot(self, plot, val):
        self.update_diff_line()
        if not self.cursor_2_val_signal_blocked:
            self.cursor_2_val_signal.emit(plot, val)

    def update_diff_line(self):
        cursor_1_pos = self.cursor_1.line.get_pos()
        cursor_2_pos = self.cursor_2.line.get_pos()
        x_diff_pos = (cursor_1_pos.x() + cursor_2_pos.x()) / 2
        y_diff_pos = (cursor_1_pos.y() + cursor_2_pos.y()) / 2
        if self.is_vertical:
            diff_pos = QtCore.QPointF(x_diff_pos, 0)
        else:
            diff_pos = QtCore.QPointF(0, y_diff_pos)
        self.diff_line.set_pos_if_finite(diff_pos)

        if self.cursor_1.on_plot_req and self.cursor_2.on_plot_req:
            self.diff_on_plot_req = True
        else:
            self.diff_on_plot_req = False
        self.update_on_plot()

        if self.diff_point_pair_to_label_function is not None:
            cursor_1_val = self.cursor_1.line.get_val()
            cursor_2_val = self.cursor_2.line.get_val()
            label_text = self.diff_point_pair_to_label_function(cursor_1_val, cursor_2_val)
            self.diff_line.set_label_text(label_text)

    def update_on_plot(self):
        if self.plot is not None:
            if self.enabled:
                if self.diff_on_plot_req and not self.diff_on_plot:
                    self.plot.addItem(self.diff_line, ignoreBounds=True)
                    self.diff_on_plot = True

                if not self.diff_on_plot_req and self.diff_on_plot:
                    self.plot.removeItem(self.diff_line)
                    self.diff_on_plot = False
            else:
                if self.diff_on_plot:
                    self.plot.removeItem(self.diff_line)
                    self.diff_on_plot = False

    def block_cursor_1_val_signal(self, block):
        self.cursor_1_val_signal_blocked = block

    def block_cursor_2_val_signal(self, block):
        self.cursor_2_val_signal_blocked = block

    def set_foreground_color(self, color):
        self.cursor_1.set_foreground_color(color)
        self.cursor_2.set_foreground_color(color)
        self.diff_line.label.setColor(color)

    def set_font(self, font):
        self.cursor_1.set_font(font)
        self.cursor_2.set_font(font)
        self.diff_line.label.setFont(font)
