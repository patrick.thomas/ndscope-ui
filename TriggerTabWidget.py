from qtpy import QtCore, QtWidgets, QtGui


class TriggerTabWidget(QtWidgets.QWidget):
    def __init__(self):
        super().__init__()

        self.trigger_enable_check_box = QtWidgets.QCheckBox('Enable')

        self.trigger_level_line_edit = QtWidgets.QLineEdit()
        self.trigger_level_line_edit.setAlignment(QtCore.Qt.AlignRight)
        self.trigger_level_line_edit.setValidator(QtGui.QDoubleValidator())
        self.trigger_level_line_edit.setFixedWidth(150)

        self.layout = QtWidgets.QFormLayout()
        self.layout.addWidget(self.trigger_enable_check_box)
        self.layout.addRow('Level:', self.trigger_level_line_edit)

        self.setLayout(self.layout)
