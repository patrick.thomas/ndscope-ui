from qtpy import QtCore, QtWidgets, QtGui
import Scope

app = QtWidgets.QApplication([])

font = QtGui.QFont('Courier')
app.setFont(font)

scope = Scope.Scope(flags=QtCore.Qt.WindowFlags())
scope.show()

if __name__ == '__main__':
    app.exec_()
