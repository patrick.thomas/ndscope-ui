from qtpy import QtCore
import pyqtgraph as pg
import numpy as np

import Transforms


class TransformableLine(pg.InfiniteLine):
    pos_changed_signal = QtCore.Signal(QtCore.QPointF)

    def __init__(self, pos=None, angle=90, pen=None, movable=False, bounds=None,
                 hoverPen=None, label=None, labelOpts=None, span=(0, 1), markers=None,
                 name=None):
        super().__init__(pos=pos, angle=angle, pen=pen, movable=movable, bounds=bounds,
                         hoverPen=hoverPen, label=label, labelOpts=labelOpts, span=span, markers=markers,
                         name=name)

        # self.transform is the name of a base class variable.
        self.transform_ = Transforms.IdTransform()

        self.val = self.get_pos()

        self.pos_changed_signal_blocked = False

        self.sigPositionChanged.connect(self.pos_changed_slot)

    def set_transform(self, transform):
        self.transform_ = transform

    def get_val(self):
        return self.val

    def set_val(self, val):
        self.val = val

    def get_pos(self):
        return QtCore.QPointF(self.getXPos(), self.getYPos())

    def set_pos(self, pos):
        self.setPos(pos)

    def set_val_if_finite(self, val):
        if np.isfinite(val.x()) and np.isfinite(val.y()):
            self.set_val(val)

    def set_pos_if_finite(self, pos):
        if np.isfinite(pos.x()) and np.isfinite(pos.y()):
            self.set_pos(pos)

    def get_val_for_pos(self, pos):
        return self.transform_.inverse_transform(pos)

    def get_pos_for_val(self, val):
        return self.transform_.transform(val)

    def block_pos_changed_signal(self, block):
        self.pos_changed_signal_blocked = block

    def pos_changed_slot(self):
        pos = self.get_pos()
        if not self.pos_changed_signal_blocked:
            self.pos_changed_signal.emit(pos)

    def set_label_text(self, label_text):
        self.label.setText(label_text)
