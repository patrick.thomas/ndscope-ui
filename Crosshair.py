from qtpy import QtCore, QtWidgets
import pyqtgraph as pg
import numpy as np


class Crosshair(QtCore.QObject):
    val_signal = QtCore.Signal(QtCore.QPointF)

    def __init__(self):
        super().__init__()
        self.hline = pg.InfiniteLine(angle=0, pen=pg.mkPen(style=QtCore.Qt.DotLine), movable=False)
        self.vline = pg.InfiniteLine(angle=90, pen=pg.mkPen(style=QtCore.Qt.DotLine), movable=False)
        self.label = pg.TextItem(anchor=(1, 1))
        self.plot = None

        self.enabled = False
        self.is_placed = False

        self.last_finite_val = None
        self.last_finite_pos = None

        self.request_on_plot = False
        self.on_plot = False

    def place(self, place):
        self.is_placed = place

    def set_enabled(self, enabled):
        self.enabled = enabled
        self.update_on_plot()

    def add_to_plot(self):
        if self.plot is not None and not self.on_plot:
            self.plot.addItem(self.hline, ignoreBounds=True)
            self.plot.addItem(self.vline, ignoreBounds=True)
            self.plot.addItem(self.label, ignoreBounds=True)
            self.on_plot = True

    def remove_from_plot(self):
        if self.plot is not None and self.on_plot:
            self.plot.removeItem(self.hline)
            self.plot.removeItem(self.vline)
            self.plot.removeItem(self.label)
            self.on_plot = False

    def move_to_plot(self, plot):
        if self.plot != plot:
            self.remove_from_plot()
            self.plot = plot
            if self.enabled and self.request_on_plot:
                self.add_to_plot()

    def update_on_plot(self):
        if self.enabled:
            if self.request_on_plot:
                self.add_to_plot()
            else:
                self.remove_from_plot()
        else:
            self.remove_from_plot()

    def set_val(self, val):
        if np.isfinite(val.x()) and np.isfinite(val.y()):
            self.last_finite_val = val
            self.set_label_from_val(val)
        self.val_signal.emit(val)

    def get_last_finite_val(self):
        return self.last_finite_val

    def set_pos(self, pos):
        if np.isfinite(pos.x()) and np.isfinite(pos.y()) and self.plot is not None:
            self.last_finite_pos = pos

            (xmin, xmax), (ymin, ymax) = self.plot.viewRange()
            if pos.x() > (xmin + xmax) / 2:
                ax = 1
            else:
                ax = 0
            if pos.y() < (ymin + ymax) / 2:
                ay = 1
            else:
                ay = 0
            self.label.setAnchor((ax, ay))

            self.hline.setPos(pos.y())
            self.vline.setPos(pos.x())
            self.label.setPos(pos)

            self.request_on_plot = True
        else:
            self.request_on_plot = False

        self.update_on_plot()

    def get_last_finite_pos(self):
        return self.last_finite_pos()

    def set_label_from_val(self, val):
        label_text = 'Y = {:g}\nT = {:g}'.format(val.y(), val.x())
        self.label.setText(label_text)

    def set_from_val(self, val):
        if self.plot is not None:
            self.set_val(val)
            pos = self.plot.transform.transform(val)
            self.set_pos(pos)

    def set_from_pos(self, pos):
        if self.plot is not None:
            self.set_pos(pos)
            val = self.plot.transform.inverse_transform(pos)
            self.set_val(val)

    def update_from_last_finite_val(self):
        if self.plot is not None:
            val = self.get_last_finite_val()
            pos = self.plot.transform.transform(val)
            self.set_pos(pos)

    def update_from_last_finite_pos(self):
        if self.plot is not None:
            pos = self.get_last_finite_pos()
            val = self.plot.transform.inverse_transform(pos)
            self.set_val(val)

    def set_foreground_color(self, color):
        self.hline.pen.setColor(color)
        self.vline.pen.setColor(color)
        self.label.setColor(color)

    def set_font(self, font):
        self.label.setFont(font)


class CrosshairTabWidget(QtWidgets.QWidget):
    def __init__(self):
        super().__init__()

        self.cross_hair_enable_check_box = QtWidgets.QCheckBox('Enable')

        self.cross_hair_y_pos_line_edit = QtWidgets.QLineEdit()
        self.cross_hair_y_pos_line_edit.setAlignment(QtCore.Qt.AlignRight)
        self.cross_hair_y_pos_line_edit.setReadOnly(True)
        self.cross_hair_y_pos_line_edit.setFixedWidth(150)

        self.cross_hair_t_pos_line_edit = QtWidgets.QLineEdit()
        self.cross_hair_t_pos_line_edit.setAlignment(QtCore.Qt.AlignRight)
        self.cross_hair_t_pos_line_edit.setReadOnly(True)
        self.cross_hair_t_pos_line_edit.setFixedWidth(150)

        self.layout = QtWidgets.QFormLayout()
        self.layout.addWidget(self.cross_hair_enable_check_box)
        self.layout.addRow('Y:', self.cross_hair_y_pos_line_edit)
        self.layout.addRow('T:', self.cross_hair_t_pos_line_edit)

        self.setLayout(self.layout)
