from qtpy import QtCore, QtWidgets, QtGui
import pyqtgraph as pg

import Plot
import Transforms
import Crosshair
import Cursors

import TriggerTabWidget
import StyleTabWidget
import TimeTabWidget


class Scope(QtWidgets.QWidget):
    def __init__(self, flags):
        super().__init__(flags=flags)

        self.graphics_layout = pg.GraphicsLayoutWidget()
        self.plot_layout = self.graphics_layout.addLayout()
        self.graphics_layout.nextRow()
        self.t0_label = pg.LabelItem('T0')
        self.graphics_layout.addItem(self.t0_label)

        self.font = QtGui.QFont()

        self.background_color = QtGui.QColor()
        self.foreground_color = QtGui.QColor()

        self.t0 = 0

        self.t_cursors_enabled = False
        self.t_cursors_single_checked = False
        self.t_cursors_dual_checked = True

        self.row_col_to_plot_dict = {}

        self.add_plot(0, 0)

        # trigger

        self.trigger_tab_widget = TriggerTabWidget.TriggerTabWidget()
        self.trigger_tab_widget.trigger_enable_check_box.toggled.connect(self.trigger_enable_check_box_toggled_slot)
        self.trigger_tab_widget.trigger_level_line_edit.editingFinished.connect(self.trigger_level_editing_finished_slot)

        self.trigger = Cursors.SingleCursor(False, pg.mkPen(style=QtCore.Qt.DashLine))
        self.trigger.set_point_to_label_function(lambda pt: 'Trigger={:g}'.format(pt.y()))
        self.trigger.set_plot(list(self.row_col_to_plot_dict.values())[0])
        self.trigger.val_signal.connect(self.trigger_val_slot)
        self.trigger.set_transform(Transforms.IdTransform())
        self.trigger.set_val(QtCore.QPointF(0, 0))
        self.trigger.set_enabled(False)

        # crosshair

        self.cross_hair_tab_widget = Crosshair.CrosshairTabWidget()

        self.cross_hair = Crosshair.Crosshair()
        self.plot_layout.scene().sigMouseMoved.connect(self.mouse_moved)
        self.plot_layout.scene().sigMouseClicked.connect(self.mouse_clicked)
        self.cross_hair_tab_widget.cross_hair_enable_check_box.toggled.connect(self.cross_hair.set_enabled)
        self.cross_hair.val_signal.connect(self.cross_hair_val_changed_slot)

        # style

        self.style_tab_widget = StyleTabWidget.StyleTabWidget()
        self.style_tab_widget.font_size_spin_box.valueChanged.connect(self.set_font_point_size)
        self.style_tab_widget.font_size_spin_box.setValue(10)
        self.style_tab_widget.color_mode_dark_radio_button.toggled.connect(self.color_mode_dark_radio_button_toggled_slot)
        self.style_tab_widget.color_mode_light_radio_button.toggled.connect(self.color_mode_light_radio_button_toggled_slot)

        # time

        self.time_tab_widget = TimeTabWidget.TimeTabWidget()

        # tabs

        self.tab_widget = QtWidgets.QTabWidget()
        self.tab_widget.addTab(self.trigger_tab_widget, 'Trigger')
        self.tab_widget.addTab(self.cross_hair_tab_widget, 'Crosshair')
        self.tab_widget.addTab(self.style_tab_widget, 'Style')
        self.tab_widget.addTab(self.time_tab_widget, 'Time')
        self.tab_widget.setSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Minimum)

        self.layout = QtWidgets.QVBoxLayout()
        self.layout.addWidget(self.graphics_layout)
        self.layout.addWidget(self.tab_widget)
        self.setLayout(self.layout)

        self.set_dark_mode()

    # x axis linking

    def reset_x_links(self):
        for plot in list(self.row_col_to_plot_dict.values())[1:]:
            plot.setXLink(list(self.row_col_to_plot_dict.values())[0])

    # plots

    def get_next_col_in_row(self, row, col):
        while (row, col) in self.row_col_to_plot_dict:
            col = col + 1
        return row, col

    def get_next_row_in_col(self, row, col):
        while (row, col) in self.row_col_to_plot_dict:
            row = row + 1
        return row, col

    def add_plot(self, row, col):
        if (row, col) not in self.row_col_to_plot_dict:
            plot = Plot.Plot(self, row, col)

            plot.set_foreground_color(self.foreground_color)
            plot.set_font(self.font)

            plot.log_mode_toggled.connect(self.log_mode_toggled_slot)

            plot.menu.cursors_widget.t_cursors_enabled_check_box.clicked.connect(self.t_cursors_enabled_check_box_clicked_slot)
            plot.menu.cursors_widget.t_cursors_single_radio_button.toggled.connect(self.t_cursors_single_radio_button_toggled_slot)
            plot.menu.cursors_widget.t_cursors_dual_radio_button.toggled.connect(self.t_cursors_dual_radio_button_toggled_slot)

            plot.t_single_cursor.val_signal.connect(self.t_single_cursor_val_slot)
            plot.t_dual_cursors.cursor_1_val_signal.connect(self.t_dual_cursors_cursor_1_val_slot)
            plot.t_dual_cursors.cursor_2_val_signal.connect(self.t_dual_cursors_cursor_2_val_slot)
            plot0 = self.row_col_to_plot_dict.get((0, 0))
            if plot0 is not None:
                cursor_1_val = plot0.t_dual_cursors.cursor_1.line.get_val()
                cursor_2_val = plot0.t_dual_cursors.cursor_2.line.get_val()
                plot.t_dual_cursors.set_cursor_1_val(cursor_1_val)
                plot.t_dual_cursors.set_cursor_2_val(cursor_2_val)

            plot.menu.cursors_widget.t_cursors_enabled_check_box.setChecked(self.t_cursors_enabled)
            plot.menu.cursors_widget.t_cursors_single_radio_button.setChecked(self.t_cursors_single_checked)
            plot.menu.cursors_widget.t_cursors_dual_radio_button.setChecked(self.t_cursors_dual_checked)

            self.row_col_to_plot_dict[(row, col)] = plot

            self.reset_x_links()

            self.plot_layout.addItem(plot, row, col)

    def add_plot_to_row(self, row, col):
        row, col = self.get_next_col_in_row(row, col)
        self.add_plot(row, col)

    def add_plot_to_col(self, row, col):
        row, col = self.get_next_row_in_col(row, col)
        self.add_plot(row, col)

    def remove_plot(self, row, col):
        if (row, col) in self.row_col_to_plot_dict:
            plot = self.row_col_to_plot_dict[(row, col)]

            plot.log_mode_toggled.disconnect()

            plot.menu.cursors_widget.t_cursors_enabled_check_box.clicked.disconnect()
            plot.menu.cursors_widget.t_cursors_single_radio_button.toggled.disconnect()
            plot.menu.cursors_widget.t_cursors_dual_radio_button.toggled.disconnect()

            plot.t_single_cursor.val_signal.disconnect()
            plot.t_dual_cursors.cursor_1_val_signal.disconnect()
            plot.t_dual_cursors.cursor_2_val_signal.disconnect()

            self.plot_layout.removeItem(plot)
            del self.row_col_to_plot_dict[(row, col)]

            self.reset_x_links()

    # t cursors

    def t_cursors_enabled_check_box_clicked_slot(self, checked):
        self.t_cursors_enabled = checked
        for plot in self.row_col_to_plot_dict.values():
            plot.menu.cursors_widget.t_cursors_enabled_check_box.setChecked(checked)

    def t_cursors_single_radio_button_toggled_slot(self, checked):
        self.t_cursors_single_checked = checked
        for plot in self.row_col_to_plot_dict.values():
            plot.menu.cursors_widget.t_cursors_single_radio_button.setChecked(checked)

    def t_cursors_dual_radio_button_toggled_slot(self, checked):
        self.t_cursors_dual_checked = checked
        for plot in self.row_col_to_plot_dict.values():
            plot.menu.cursors_widget.t_cursors_dual_radio_button.setChecked(checked)

    def t_single_cursor_val_slot(self, source_plot, val):
        for plot in self.row_col_to_plot_dict.values():
            if plot != source_plot:
                plot.t_single_cursor.block_val_signal(True)
                plot.t_single_cursor.set_val(val)
                plot.t_single_cursor.block_val_signal(False)

    def t_dual_cursors_cursor_1_val_slot(self, source_plot, val):
        for plot in self.row_col_to_plot_dict.values():
            if plot != source_plot:
                plot.t_dual_cursors.block_cursor_1_val_signal(True)
                plot.t_dual_cursors.set_cursor_1_val(val)
                plot.t_dual_cursors.block_cursor_1_val_signal(False)

    def t_dual_cursors_cursor_2_val_slot(self, source_plot, val):
        for plot in self.row_col_to_plot_dict.values():
            if plot != source_plot:
                plot.t_dual_cursors.block_cursor_2_val_signal(True)
                plot.t_dual_cursors.set_cursor_2_val(val)
                plot.t_dual_cursors.block_cursor_2_val_signal(False)

    # log mode

    def log_mode_toggled_slot(self, plot, checked):
        if plot == self.trigger.plot:
            if checked:
                transform = Transforms.LogYTransform()
                self.trigger.set_transform(transform)
            else:
                transform = Transforms.IdTransform()
                self.trigger.set_transform(transform)
        self.cross_hair.update_from_last_finite_val()

    # trigger

    def trigger_enable_check_box_toggled_slot(self, checked):
        self.trigger.set_enabled(checked)

    def trigger_level_editing_finished_slot(self):
        self.trigger.set_val(QtCore.QPointF(0, float(self.trigger_tab_widget.trigger_level_line_edit.text())))

    def trigger_val_slot(self, _, val):
        text = '{:g}'.format(val.y())
        self.trigger_tab_widget.trigger_level_line_edit.setText(text)

    # crosshair

    def mouse_moved(self, pos):
        for plot in self.row_col_to_plot_dict.values():
            if plot.getViewBox().sceneBoundingRect().contains(pos):
                mapped_pos = plot.vb.mapSceneToView(pos)
                if not self.cross_hair.is_placed:
                    self.cross_hair.move_to_plot(plot)
                    self.cross_hair.set_from_pos(mapped_pos)

    def mouse_clicked(self, event):
        if event.button() == QtCore.Qt.LeftButton:
            self.cross_hair.place(not self.cross_hair.is_placed)

    def cross_hair_val_changed_slot(self, val):
        y_pos_text = '{:g}'.format(val.y())
        self.cross_hair_tab_widget.cross_hair_y_pos_line_edit.setText(y_pos_text)

        t_pos_text = '{:g}'.format(val.x())
        self.cross_hair_tab_widget.cross_hair_t_pos_line_edit.setText(t_pos_text)

    # font

    def set_font(self, font):
        self.font = font
        self.cross_hair.set_font(font)
        self.trigger.set_font(font)

        for plot in self.row_col_to_plot_dict.values():
            plot.set_font(self.font)

    def set_font_point_size(self, point_size):
        self.t0_label.setText(self.t0_label.text, size=str(point_size + 10) + 'pt')
        font = self.font
        font.setPointSize(point_size)
        self.set_font(font)

    # color mode

    def set_background_color(self, color):
        self.background_color = color
        self.graphics_layout.setBackground(color)

    def set_foreground_color(self, color):
        self.foreground_color = color
        self.t0_label.setText(self.t0_label.text, color=color)
        self.cross_hair.set_foreground_color(color)
        self.trigger.set_foreground_color(color)

        for plot in self.row_col_to_plot_dict.values():
            plot.set_foreground_color(color)

    def set_dark_mode(self):
        self.set_background_color(QtGui.QColor('black'))
        self.set_foreground_color(QtGui.QColor('white'))

    def set_light_mode(self):
        self.set_background_color(QtGui.QColor('white'))
        self.set_foreground_color(QtGui.QColor('black'))

    def color_mode_dark_radio_button_toggled_slot(self, checked):
        if checked:
            self.set_dark_mode()

    def color_mode_light_radio_button_toggled_slot(self, checked):
        if checked:
            self.set_light_mode()
