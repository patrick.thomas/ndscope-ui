import argparse
import sys

import nds2

from qtpy import QtGui, QtWidgets

import Channel

import ChannelsDialog


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    # h1nds0 8088
    # nds.ligo-wa.caltech.edu
    # nds.ligo.caltech.edu
    parser.add_argument('--host', metavar='HOST', default='nds.ligo-wa.caltech.edu', help='nds host')
    parser.add_argument('--port', metavar='PORT', type=int, default=31200, help='nds port')
    parser.add_argument('--test', action='store_true')

    args = parser.parse_args()

    if args.test:
        sorted_channel_list = Channel.get_test_channel_list()
    else:
        print(f'Connecting to {args.host}:{args.port}.')
        conn = nds2.connection(args.host, args.port)
        print('Done.')

        print('Fetching channel list.')
        channel_mask = nds2.channel.CHANNEL_TYPE_ONLINE | nds2.channel.CHANNEL_TYPE_RAW | nds2.channel.CHANNEL_TYPE_RDS | nds2.channel.CHANNEL_TYPE_TEST_POINT
        nds_channel_list = conn.find_channels('*', channel_mask)
        print('Done.')

        print('Closing connection.')
        conn.close()
        print('Done.')

        print('Grouping channels by name.')
        channel_list_dict = {}
        for nds_channel in nds_channel_list:
            if nds_channel.name not in channel_list_dict:
                channel_list_dict[nds_channel.name] = Channel.Channel.from_nds_channel(nds_channel)
            else:
                current = channel_list_dict[nds_channel.name]
                current.add_from_nds_channel(nds_channel)
        print('Done.')

        print('Sorting channels by name.')
        sorted_channel_list = sorted(channel_list_dict.values(), key=lambda x: x.name)
        print('Done.')

    app = QtWidgets.QApplication([])

    f = QtGui.QFont('Courier')
    f.setPointSize(12)
    app.setFont(f)

    dialog = ChannelsDialog.ChannelsDialog(sorted_channel_list, 5)
    dialog.show()

    sys.exit(app.exec_())
