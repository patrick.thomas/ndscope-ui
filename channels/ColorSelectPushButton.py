from qtpy import QtCore, QtWidgets, QtGui


# A push button that holds a color and a color dialog to set the color.
# A rectangle of the color is painted in the center of the push button.
# The color dialog is opened when the push button is clicked.
class ColorSelectPushButton(QtWidgets.QPushButton):
    def __init__(self, color=QtGui.QColor()):
        super().__init__()

        self.color = color
        self.color_dialog = QtWidgets.QColorDialog()

        self.clicked.connect(self.dialog_get_color)

    def dialog_get_color(self):
        color = self.color_dialog.getColor(initial=self.color, parent=self, title='Select Color')

        if color.isValid():
            self.color = color

    def get_color(self):
        return self.color

    def paintEvent(self, event):
        super().paintEvent(event)

        # Adds a rectangle of the selected color to the center of the push button.
        r_side_length = min(self.width(), self.height()) * 0.8
        r = QtCore.QRectF(0, 0, r_side_length, r_side_length)
        r.moveTo(QtCore.QRectF(self.rect()).center() - r.center())
        painter = QtGui.QPainter(self)
        painter.setPen(self.color)
        painter.setBrush(self.color)
        painter.drawRect(r)
