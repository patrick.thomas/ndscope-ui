import Channel

from qtpy import QtWidgets, QtCore


class SelectFilterWidget(QtWidgets.QWidget):
    def __init__(self, parent=None, flags=QtCore.Qt.WindowFlags()):
        super().__init__(parent=parent, flags=flags)

        self.all_radio_button = QtWidgets.QRadioButton('All')
        self.all_radio_button.setChecked(True)

        self.slow_radio_button = QtWidgets.QRadioButton('Slow Only')

        self.fast_radio_button = QtWidgets.QRadioButton('Fast Only')

        self.sample_rate_radio_button_layout = QtWidgets.QHBoxLayout()
        self.sample_rate_radio_button_layout.addWidget(self.all_radio_button)
        self.sample_rate_radio_button_layout.addWidget(self.slow_radio_button)
        self.sample_rate_radio_button_layout.addWidget(self.fast_radio_button)

        self.sample_rate_radio_button_group = QtWidgets.QGroupBox('Sample Rate')
        self.sample_rate_radio_button_group.setLayout(self.sample_rate_radio_button_layout)

        self.online_check_box = QtWidgets.QCheckBox('Online Only')
        self.online_check_box.setStyleSheet(f'QCheckBox {{ color: {Channel.COLOR_ONLINE} }}')

        self.layout = QtWidgets.QHBoxLayout()
        self.layout.addWidget(self.sample_rate_radio_button_group)
        self.layout.addWidget(self.online_check_box)

        self.setLayout(self.layout)
