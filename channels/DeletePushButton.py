from qtpy import QtCore, QtWidgets, QtGui


class DeletePushButton(QtWidgets.QPushButton):
    def paintEvent(self, event):
        super().paintEvent(event)

        r = QtCore.QRectF(0, 0, self.width() * 0.5, self.height() * 0.3)
        r.moveTo(QtCore.QRectF(self.rect()).center() - r.center())
        painter = QtGui.QPainter(self)
        painter.setPen(QtGui.QColor('red'))
        painter.setBrush(QtGui.QColor('red'))
        painter.drawRect(r)
