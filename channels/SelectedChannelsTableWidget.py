from qtpy import QtCore, QtWidgets, QtGui

import DeletePushButton
import ColorSelectPushButton


# https://stackoverflow.com/questions/64198197/how-to-prevent-too-aggressive-text-elide-in-qtableview
class ElideDelegate(QtWidgets.QStyledItemDelegate):
    def paint(self, painter, option, index):
        if not index.isValid():
            return

        opt = option
        QtWidgets.QStyledItemDelegate.initStyleOption(self, opt, index)
        padding = 3

        painter.save()
        painter.setClipRect(opt.rect)
        opt.rect = opt.rect.adjusted(padding, padding, -padding, -padding)
        painter.drawText(opt.rect, QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter, opt.fontMetrics.elidedText(opt.text, QtCore.Qt.ElideRight, opt.rect.width()))
        painter.restore()

        opt.rect = option.rect
        opt.textElideMode = QtCore.Qt.ElideNone
        opt.text = ''

        QtWidgets.QApplication.style().drawControl(QtWidgets.QStyle.CE_ItemViewItem, opt, painter)


class SelectedChannelsTableWidget(QtWidgets.QTableWidget):
    def __init__(self):
        super().__init__()

        self.setColumnCount(5)
        self.setColumnWidth(1, 200)
        self.setHorizontalHeaderLabels(['', 'name', 'color', 'scale', 'offset'])
        self.horizontalHeader().setSectionResizeMode(0, QtWidgets.QHeaderView.ResizeToContents)
        self.horizontalHeader().setSectionResizeMode(1, QtWidgets.QHeaderView.Interactive)
        self.horizontalHeader().setSectionResizeMode(2, QtWidgets.QHeaderView.ResizeToContents)
        self.horizontalHeader().setSectionResizeMode(3, QtWidgets.QHeaderView.Interactive)
        self.horizontalHeader().setSectionResizeMode(4, QtWidgets.QHeaderView.Interactive)
        self.horizontalHeader().setHighlightSections(False)

        self.setDragDropMode(QtWidgets.QAbstractItemView.DropOnly)
        self.setDefaultDropAction(QtCore.Qt.MoveAction)
        self.setDropIndicatorShown(True)

        self.elide_delegate = ElideDelegate()
        self.setItemDelegateForColumn(1, self.elide_delegate)

    def dropMimeData(self, row, column, data, action):
        if data.hasFormat('text/plain'):
            text_list = data.text().splitlines()
            item = QtWidgets.QTableWidgetItem(text_list[0])
            self.setItem(row, column, item)
            return True
        else:
            return super().dropMimeData(row, column, data, action)

    def dragMoveEvent(self, event):
        event.accept()

    def dragEnterEvent(self, event):
        event.accept()

    def append_row(self):
        self.insertRow(self.rowCount())

        delete_push_button = DeletePushButton.DeletePushButton()
        self.setCellWidget(self.rowCount() - 1, 0, delete_push_button)

# Drop does not replace contents.
#        name_line_edit = QtWidgets.QLineEdit()
#        self.setCellWidget(self.rowCount() - 1, 1, name_line_edit)

        color_select_push_button = ColorSelectPushButton.ColorSelectPushButton()
        self.setCellWidget(self.rowCount() - 1, 2, color_select_push_button)

        scale_line_edit = QtWidgets.QLineEdit()
        scale_line_edit.setValidator(QtGui.QDoubleValidator())
        scale_line_edit.setAlignment(QtCore.Qt.AlignRight)
        scale_line_edit.setText('1.0')
        self.setCellWidget(self.rowCount() - 1, 3, scale_line_edit)

        offset_line_edit = QtWidgets.QLineEdit()
        offset_line_edit.setValidator(QtGui.QDoubleValidator())
        offset_line_edit.setAlignment(QtCore.Qt.AlignRight)
        offset_line_edit.setText('0.0')
        self.setCellWidget(self.rowCount() - 1, 4, offset_line_edit)

    def append_rows(self, count):
        for i in range(count):
            self.append_row()

    def get_channel_name_list(self):
        channel_name_list = []
        for row in range(self.rowCount()):
            item = self.item(row, 1)
            if item:
                channel_name = item.data(QtCore.Qt.DisplayRole)
                channel_name_list.append(channel_name)
        return channel_name_list
