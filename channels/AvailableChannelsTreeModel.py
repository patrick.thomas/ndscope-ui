import re

from qtpy import QtCore


class TreeItem:
    def __init__(self, parent=None, data=None, is_leaf=False):
        self.parent = parent
        self.data = data
        self.is_leaf = is_leaf
        if not self.is_leaf:
            self.branch_dict = {}
            self.leaf_dict = {}

            self.has_slow = False
            self.has_slow_online = False
            self.has_fast = False
            self.has_fast_online = False

    def child_list(self):
        if not self.is_leaf:
            return list(self.branch_dict.values()) + list(self.leaf_dict.values())
        return []

    def row(self):
        if self.parent:
            return self.parent.child_list().index(self)
        else:
            return 0


class AvailableChannelsTreeModel(QtCore.QAbstractItemModel):
    def __init__(self, channel_list, max_depth):
        super().__init__()
        self.root = TreeItem()
        self.header_role_data = {QtCore.Qt.DisplayRole: ('name', 'sample rate [Hz]'),
                                 QtCore.Qt.TextAlignmentRole: (QtCore.Qt.AlignCenter, QtCore.Qt.AlignCenter)}

        self.insert(channel_list, max_depth)

    def insert(self, channel_list, max_depth):
        split_re = re.compile('[:\-_]')
        for channel in channel_list:
            current = self.root
            slow = channel.slow
            online = channel.online
            name_part_list = list(filter(None, split_re.split(channel.name, max_depth)))
            for name_part in name_part_list[:-1]:
                if name_part not in current.branch_dict:
                    current.branch_dict[name_part] = TreeItem(current, name_part)
                current = current.branch_dict[name_part]
                if slow:
                    current.has_slow = True
                    if online:
                        current.has_slow_online = True
                else:
                    current.has_fast = True
                    if online:
                        current.has_fast_online = True
            current.leaf_dict[channel.name] = TreeItem(current, channel, is_leaf=True)

    def columnCount(self, parent=QtCore.QModelIndex()):
        return len(self.header_role_data[QtCore.Qt.DisplayRole])

    def data(self, index, role=QtCore.Qt.DisplayRole):
        if not index.isValid():
            return QtCore.QVariant()

        item = index.internalPointer()
        if item.is_leaf:
            channel = item.data
            sample_rate = ', '.join(f'{float(sample_rate):g}' for sample_rate in channel.sample_rate_set)
            if channel.test_point:
                sample_rate += ' [TP]'
            role_data = {QtCore.Qt.DisplayRole: (channel.name, sample_rate),
                         QtCore.Qt.ForegroundRole: (channel.get_brush(), channel.get_brush()),
                         QtCore.Qt.TextAlignmentRole: (QtCore.Qt.AlignLeft, QtCore.Qt.AlignRight)}
        else:
            name_part = item.data
            role_data = {QtCore.Qt.DisplayRole: (name_part, ''),
                         QtCore.Qt.TextAlignmentRole: (QtCore.Qt.AlignLeft, QtCore.Qt.AlignRight)}

        try:
            return role_data[role][index.column()]
        except KeyError:
            return QtCore.QVariant()
        except IndexError:
            return QtCore.QVariant()

    def flags(self, index):
        if not index.isValid():
            return QtCore.Qt.NoItemFlags

        item = index.internalPointer()
        if item.is_leaf:
            flags = (
                QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsDragEnabled | QtCore.Qt.ItemNeverHasChildren,
                QtCore.Qt.ItemIsEnabled)
        else:
            flags = (QtCore.Qt.ItemIsEnabled, QtCore.Qt.ItemIsEnabled)

        try:
            return flags[index.column()]
        except IndexError:
            return QtCore.Qt.NoItemFlags

    def headerData(self, section, orientation, role=QtCore.Qt.DisplayRole):
        if orientation == QtCore.Qt.Horizontal:
            try:
                return self.header_role_data[role][section]
            except KeyError:
                return QtCore.QVariant()
            except IndexError:
                return QtCore.QVariant()

        return QtCore.QVariant()

    def index(self, row, column, parent=QtCore.QModelIndex()):
        if not self.hasIndex(row, column, parent):
            return QtCore.QModelIndex()

        if not parent.isValid():
            parent_item = self.root
        else:
            parent_item = parent.internalPointer()

        try:
            child_item = parent_item.child_list()[row]
            return self.createIndex(row, column, child_item)
        except IndexError:
            return QtCore.QModelIndex()

    def mimeData(self, indexes):
        text_list = [self.data(index, QtCore.Qt.DisplayRole) for index in indexes if index.isValid()]
        text = '\n'.join(text_list)
        mime_data = QtCore.QMimeData()
        mime_data.setText(text)
        return mime_data

    def mimeTypes(self):
        return ['text/plain']

    def parent(self, index):
        if not index.isValid():
            return QtCore.QModelIndex()

        child_item = index.internalPointer()
        parent_item = child_item.parent
        if parent_item == self.root:
            return QtCore.QModelIndex()

        row = parent_item.row()
        return self.createIndex(row, 0, parent_item)

    def rowCount(self, parent=QtCore.QModelIndex()):
        if parent.column() > 0:
            return 0

        if not parent.isValid():
            parent_item = self.root
        else:
            parent_item = parent.internalPointer()

        return len(parent_item.child_list())


# Filters the branch and leaf items based on the sample rate and online status
# of the channels that they contain.
class AvailableChannelsTreeFilterProxyModel(QtCore.QSortFilterProxyModel):
    def __init__(self):
        super().__init__()
        self.slow_only = False
        self.fast_only = False
        self.online_only = False

    def all_radio_button_toggled_slot(self, checked):
        if checked:
            self.slow_only = False
            self.fast_only = False
            self.invalidateFilter()

    def slow_radio_button_toggled_slot(self, checked):
        if checked:
            self.slow_only = True
            self.fast_only = False
            self.invalidateFilter()

    def fast_radio_button_toggled_slot(self, checked):
        if checked:
            self.slow_only = False
            self.fast_only = True
            self.invalidateFilter()

    def online_check_box_state_changed_slot(self, state):
        if state == QtCore.Qt.Checked:
            self.online_only = True
        else:
            self.online_only = False
        self.invalidateFilter()

    def filterAcceptsRow(self, source_row, source_parent):
        index = self.sourceModel().index(source_row, 0, source_parent)
        assert self.sourceModel().checkIndex(index)
        item = index.internalPointer()
        if item.is_leaf:
            channel = item.data
            if self.online_only and not channel.online:
                return False
            if self.slow_only and not channel.slow:
                return False
            if self.fast_only and not channel.fast:
                return False
        else:
            if self.online_only and self.slow_only and not item.has_slow_online:
                return False
            if self.online_only and self.fast_only and not item.has_fast_online:
                return False
            if self.online_only and not (item.has_slow_online or item.has_fast_online):
                return False
            if self.slow_only and not item.has_slow:
                return False
            if self.fast_only and not item.has_fast:
                return False

        return True
