import time

import AvailableChannelsTableModel
import AvailableChannelsTreeModel
import SelectFilterWidget

import SelectedChannelsTableWidget

from qtpy import QtWidgets, QtCore


class ChannelsWidget(QtWidgets.QWidget):
    def __init__(self, channel_list, depth, parent=None, flags=QtCore.Qt.WindowFlags()):
        super().__init__(parent=parent, flags=flags)

        # available channels table model
        self.available_channels_table_model = AvailableChannelsTableModel.AvailableChannelsTableModel(channel_list)

        # available channels table view
        self.available_channels_table_view = QtWidgets.QTableView()
        self.available_channels_table_view.setModel(self.available_channels_table_model)
        self.available_channels_table_view.horizontalHeader().setSectionResizeMode(0, QtWidgets.QHeaderView.Stretch)
        self.available_channels_table_view.horizontalHeader().setSectionResizeMode(1, QtWidgets.QHeaderView.ResizeToContents)
        self.available_channels_table_view.horizontalHeader().setHighlightSections(False)
        self.available_channels_table_view.setDragEnabled(True)
        # Setting the selection mode to single selection avoids the potential for
        # freezing the application if all of the items are selected.
        self.available_channels_table_view.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)
        self.available_channels_table_view.setWordWrap(False)

        self.search_pattern_line_edit_widget = QtWidgets.QLineEdit()
        self.search_pattern_line_edit_widget.setPlaceholderText('Substring Search')
        self.search_pattern_line_edit_widget.textChanged.connect(self.available_channels_table_model.search_pattern_changed_slot)

        self.available_channels_table_page = QtWidgets.QWidget(flags=QtCore.Qt.WindowFlags())
        self.available_channels_table_page_layout = QtWidgets.QVBoxLayout()
        self.available_channels_table_page_layout.addWidget(self.search_pattern_line_edit_widget)
        self.available_channels_table_page_layout.addWidget(self.available_channels_table_view)
        self.available_channels_table_page.setLayout(self.available_channels_table_page_layout)

        # available channels tree model
        print(f"Creating tree model for {len(channel_list)} channels.")
        t0 = time.perf_counter()
        self.available_channels_tree_model = AvailableChannelsTreeModel.AvailableChannelsTreeModel(channel_list, depth)
        t1 = time.perf_counter()
        diff = t1 - t0
        print(f"Created tree in {diff} seconds.")

        # available channels tree filter proxy model
        self.available_channels_tree_filter_proxy_model = AvailableChannelsTreeModel.AvailableChannelsTreeFilterProxyModel()
        self.available_channels_tree_filter_proxy_model.setSourceModel(self.available_channels_tree_model)

        # available channels tree view
        self.available_channels_tree_view = QtWidgets.QTreeView()
        self.available_channels_tree_view.setModel(self.available_channels_tree_filter_proxy_model)
        self.available_channels_tree_view.setDragEnabled(True)
        self.available_channels_tree_view.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.available_channels_tree_view.header().setSectionResizeMode(0, QtWidgets.QHeaderView.ResizeToContents)
        self.available_channels_tree_view.header().setSectionResizeMode(1, QtWidgets.QHeaderView.ResizeToContents)
        self.available_channels_tree_view.header().setStretchLastSection(False)
        self.available_channels_tree_view.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)

        # available channels tab widget
        self.available_channels_tab_widget = QtWidgets.QTabWidget()
        self.available_channels_tab_widget.addTab(self.available_channels_table_page, 'Table')
        self.available_channels_tab_widget.addTab(self.available_channels_tree_view, 'Tree')

        self.select_filter_widget = SelectFilterWidget.SelectFilterWidget()

        self.select_filter_widget.all_radio_button.toggled.connect(self.available_channels_table_model.all_radio_button_toggled_slot)
        self.select_filter_widget.all_radio_button.toggled.connect(self.available_channels_tree_filter_proxy_model.all_radio_button_toggled_slot)

        self.select_filter_widget.slow_radio_button.toggled.connect(self.available_channels_table_model.slow_radio_button_toggled_slot)
        self.select_filter_widget.slow_radio_button.toggled.connect(self.available_channels_tree_filter_proxy_model.slow_radio_button_toggled_slot)

        self.select_filter_widget.fast_radio_button.toggled.connect(self.available_channels_table_model.fast_radio_button_toggled_slot)
        self.select_filter_widget.fast_radio_button.toggled.connect(self.available_channels_tree_filter_proxy_model.fast_radio_button_toggled_slot)

        self.select_filter_widget.online_check_box.stateChanged.connect(self.available_channels_table_model.online_check_box_state_changed_slot)
        self.select_filter_widget.online_check_box.stateChanged.connect(self.available_channels_tree_filter_proxy_model.online_check_box_state_changed_slot)

        self.available_channels_layout = QtWidgets.QVBoxLayout()
        self.available_channels_layout.addWidget(self.available_channels_tab_widget)
        self.available_channels_layout.addWidget(self.select_filter_widget)

        # selected channels table widget
        self.selected_channels_table_widget = SelectedChannelsTableWidget.SelectedChannelsTableWidget()
        self.selected_channels_table_widget.append_rows(10)

        self.layout = QtWidgets.QHBoxLayout()
        self.layout.addLayout(self.available_channels_layout)
        self.layout.addWidget(self.selected_channels_table_widget)
        self.setLayout(self.layout)

        self.search_pattern_line_edit_widget.setFocus()

    def get_selected_channel_name_list(self):
        return self.selected_channels_table_widget.get_channel_name_list()
