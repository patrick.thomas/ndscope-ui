import copy
import itertools
import gpstime

import nds2

from qtpy import QtGui


COLOR_TEST_POINT = 'blue'
COLOR_ONLINE = 'green'


def is_slow(sample_rate):
    return sample_rate <= 16


def is_fast(sample_rate):
    return not is_slow(sample_rate)


def is_online(nds_channel):
    return nds_channel.channel_type in [nds2.channel.CHANNEL_TYPE_ONLINE, nds2.channel.CHANNEL_TYPE_TEST_POINT]


def is_test_point(nds_channel):
    return nds_channel.channel_type == nds2.channel.CHANNEL_TYPE_TEST_POINT


class Channel:
    def __init__(self, name, sample_rate, online, test_point):
        self.name = name
        self.sample_rate_set = {sample_rate}
        self.slow = is_slow(sample_rate)
        self.fast = is_fast(sample_rate)
        self.online = online
        self.test_point = test_point

    # For testing.
    @classmethod
    def from_nds_channel(cls, nds_channel):
        return cls(nds_channel.name, nds_channel.sample_rate, is_online(nds_channel), is_test_point(nds_channel))

    # For testing.
    def add_from_args(self, name, sample_rate, online, test_point):
        assert name == self.name
        self.sample_rate_set.add(sample_rate)
        self.slow |= is_slow(sample_rate)
        self.fast |= is_fast(sample_rate)
        self.online |= online
        self.test_point |= test_point

    # For testing.
    def add_from_channel(self, channel):
        assert channel.name == self.name
        self.sample_rate_set = self.sample_rate_set.union(channel.sample_rate_set)
        self.slow |= channel.slow
        self.fast |= channel.fast
        self.online |= channel.online
        self.test_point |= channel.test_point

    def add_from_nds_channel(self, nds_channel):
        self.add_from_args(nds_channel.name, nds_channel.sample_rate, is_online(nds_channel), is_test_point(nds_channel))

    def get_brush(self):
        if self.test_point:
            return QtGui.QBrush(QtGui.QColor(COLOR_TEST_POINT))
        elif self.online:
            return QtGui.QBrush(QtGui.QColor(COLOR_ONLINE))
        else:
            return QtGui.QBrush()

    # For testing
    def add_prefix(self, prefix):
        self.name = prefix + self.name


def get_test_channel_list():
    base_channel_list = [
        Channel('A', 16, False, False),
        Channel('B', 16, True, False),
        Channel('C', 32, False, False),
        Channel('D', 32, True, False),
        Channel('E', 16, False, True),
        Channel('F', 16, True, True),
        Channel('G', 32, False, True),
        Channel('H', 32, True, True)
    ]

    group_list = []
    for i in range(len(base_channel_list)):
        group_list += [list(copy.deepcopy(c)) for c in itertools.combinations(base_channel_list, i + 1)]

    i = 1
    channel_list = []
    for group in group_list:
        for channel in group:
            channel.add_prefix(f'{i}_')
            channel_list.append(channel)
        i += 1

    return channel_list


class TrendChannelGroup:
    def __init__(self, label, mean_buffer, min_buffer, max_buffer):
        assert mean_buffer.sample_rate == min_buffer.sample_rate == max_buffer.sample_rate
        assert mean_buffer.gps_seconds == min_buffer.gps_seconds == max_buffer.gps_seconds
        assert len(mean_buffer.data) == len(min_buffer.data) == len(max_buffer.data)

        self.label = label
        self.mean_label = mean_buffer.name
        self.min_label = min_buffer.name
        self.max_label = max_buffer.name

        sample_rate = mean_buffer.sample_rate
        gps_seconds = mean_buffer.gps_seconds
        count = len(mean_buffer.data)
        t_gps = [gps_seconds + i / sample_rate for i in range(count)]
        self.t = [gpstime.gpstime.fromgps(t) for t in t_gps]

        self.mean_data = mean_buffer.data
        self.min_data = min_buffer.data
        self.max_data = max_buffer.data


def get_gps_seconds_array_for_nds_buffer(nds_buffer):
    return [nds_buffer.gps_seconds + (i / nds_buffer.sample_rate) for i in range(len(nds_buffer.data))]
