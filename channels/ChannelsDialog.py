from qtpy import QtWidgets, QtCore

import ChannelsWidget


class ChannelsDialog(QtWidgets.QDialog):
    def __init__(self, channel_list, depth, parent=None, flags=QtCore.Qt.WindowFlags()):
        super().__init__(parent=parent, flags=flags)

        self.channels_widget = ChannelsWidget.ChannelsWidget(channel_list, depth, parent, flags)

        self.dialog_button_box = QtWidgets.QDialogButtonBox(QtWidgets.QDialogButtonBox.Ok | QtWidgets.QDialogButtonBox.Cancel)
        self.dialog_button_box.accepted.connect(self.accept)
        self.dialog_button_box.rejected.connect(self.reject)

        self.layout = QtWidgets.QVBoxLayout()
        self.layout.addWidget(self.channels_widget)
        self.layout.addWidget(self.dialog_button_box)
        self.setLayout(self.layout)

    def get_selected_channel_name_list(self):
        return self.channels_widget.get_selected_channel_name_list()
