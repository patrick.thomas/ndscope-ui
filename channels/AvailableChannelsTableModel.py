from qtpy import QtCore


class AvailableChannelsTableModel(QtCore.QAbstractTableModel):
    def __init__(self, channel_list, parent=None):
        super().__init__(parent)
        self.search_pattern = ''
        self.slow_only = False
        self.fast_only = False
        self.online_only = False

        self.full_channel_list = channel_list
        self.filtered_channel_list = channel_list

        self.header_role_data = {QtCore.Qt.DisplayRole: ('name', 'sample rate [Hz]'),
                                 QtCore.Qt.TextAlignmentRole: (QtCore.Qt.AlignCenter, QtCore.Qt.AlignCenter)}

    def columnCount(self, parent=QtCore.QModelIndex()):
        return len(self.header_role_data[QtCore.Qt.DisplayRole])

    def data(self, index, role=QtCore.Qt.DisplayRole):
        if not index.isValid():
            return QtCore.QVariant()

        channel = self.filtered_channel_list[index.row()]
        sample_rate = ', '.join(f'{float(sample_rate):g}' for sample_rate in channel.sample_rate_set)
        if channel.test_point:
            sample_rate += ' [TP]'
        role_data = {QtCore.Qt.DisplayRole: (channel.name, sample_rate),
                     QtCore.Qt.ForegroundRole: (channel.get_brush(), channel.get_brush()),
                     QtCore.Qt.TextAlignmentRole: (QtCore.Qt.AlignLeft, QtCore.Qt.AlignRight)}

        try:
            return role_data[role][index.column()]
        except KeyError:
            return QtCore.QVariant()
        except IndexError:
            return QtCore.QVariant()

    def flags(self, index):
        if not index.isValid():
            return QtCore.Qt.NoItemFlags

        flags = (
            (QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsDragEnabled | QtCore.Qt.ItemNeverHasChildren),
            (QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemNeverHasChildren),
        )

        try:
            return flags[index.column()]
        except IndexError:
            return QtCore.Qt.NoItemFlags

    def headerData(self, section, orientation, role=QtCore.Qt.DisplayRole):
        if orientation == QtCore.Qt.Horizontal:
            try:
                return self.header_role_data[role][section]
            except KeyError:
                return QtCore.QVariant()
            except IndexError:
                return QtCore.QVariant()
        elif orientation == QtCore.Qt.Vertical:
            return section + 1

        return QtCore.QVariant()

    def mimeData(self, indexes):
        text_list = [self.data(index, QtCore.Qt.DisplayRole) for index in indexes if index.isValid()]
        text = '\n'.join(text_list)
        mime_data = QtCore.QMimeData()
        mime_data.setText(text)
        return mime_data

    def mimeTypes(self):
        return ['text/plain']

    def rowCount(self, parent=QtCore.QModelIndex()):
        if parent.isValid():
            return 0
        return len(self.filtered_channel_list)

    def search_pattern_changed_slot(self, pattern):
        self.search_pattern = pattern
        self.update_filter()

    def all_radio_button_toggled_slot(self, checked):
        if checked:
            self.slow_only = False
            self.fast_only = False
            self.update_filter()

    def slow_radio_button_toggled_slot(self, checked):
        if checked:
            self.slow_only = True
            self.fast_only = False
            self.update_filter()

    def fast_radio_button_toggled_slot(self, checked):
        if checked:
            self.slow_only = False
            self.fast_only = True
            self.update_filter()

    def online_check_box_state_changed_slot(self, state):
        if state == QtCore.Qt.Checked:
            self.online_only = True
        else:
            self.online_only = False
        self.update_filter()

    def update_filter(self):
        filtered_channel_list = []
        for channel in self.full_channel_list:
            if self.slow_only and not channel.slow:
                continue
            if self.fast_only and not channel.fast:
                continue
            if self.online_only and not channel.online:
                continue
            if not self.search_pattern.upper() in channel.name.upper():
                continue
            filtered_channel_list.append(channel)

        self.beginResetModel()
        self.filtered_channel_list = filtered_channel_list
        self.endResetModel()
