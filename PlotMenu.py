from qtpy import QtCore, QtWidgets, QtGui


class MouseModeWidget(QtWidgets.QWidget):
    def __init__(self):
        super().__init__()

        self.pan_zoom_radio_button = QtWidgets.QRadioButton('Pan && Zoom')
        self.pan_zoom_radio_button.setChecked(True)
        self.zoom_box_radio_button = QtWidgets.QRadioButton('Zoom Box')

        self.layout = QtWidgets.QVBoxLayout()
        self.layout.addWidget(self.pan_zoom_radio_button)
        self.layout.addWidget(self.zoom_box_radio_button)
        self.setLayout(self.layout)


class ScaleWidget(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super().__init__(parent=parent)

        self.manual_radio_button = QtWidgets.QRadioButton('Manual')
        self.manual_line_edit_1 = QtWidgets.QLineEdit()
        self.manual_line_edit_1.setValidator(QtGui.QDoubleValidator())
        self.manual_line_edit_1.setAlignment(QtCore.Qt.AlignRight)
        self.manual_line_edit_2 = QtWidgets.QLineEdit()
        self.manual_line_edit_2.setValidator(QtGui.QDoubleValidator())
        self.manual_line_edit_2.setAlignment(QtCore.Qt.AlignRight)

        self.auto_radio_button = QtWidgets.QRadioButton('Auto')
        self.auto_radio_button.setChecked(True)
        self.auto_percent_spin_box = QtWidgets.QSpinBox()
        self.auto_percent_spin_box.setMinimum(0)
        self.auto_percent_spin_box.setMaximum(100)
        self.auto_percent_spin_box.setValue(100)
        self.auto_percent_spin_box.setSuffix('%')
        self.auto_percent_spin_box.setAlignment(QtCore.Qt.AlignRight)

        self.log_mode_check_box = QtWidgets.QCheckBox('Log')

        self.layout = QtWidgets.QGridLayout()
        self.layout.addWidget(self.manual_radio_button, 0, 0)
        self.layout.addWidget(self.manual_line_edit_1, 0, 1)
        self.layout.addWidget(self.manual_line_edit_2, 0, 2)
        self.layout.addWidget(self.auto_radio_button, 1, 0)
        self.layout.addWidget(self.auto_percent_spin_box, 1, 1)
        self.layout.addWidget(self.log_mode_check_box, 2, 0)
        self.setLayout(self.layout)


class CursorsWidget(QtWidgets.QWidget):
    def __init__(self):
        super().__init__()

        self.y_cursors_enabled_check_box = QtWidgets.QCheckBox('Y Cursors')
        self.y_cursors_single_radio_button = QtWidgets.QRadioButton('Single')
        self.y_cursors_dual_radio_button = QtWidgets.QRadioButton('Dual')
        self.y_cursors_dual_radio_button.setChecked(True)
        self.y_cursors_single_dual_radio_button_group = QtWidgets.QButtonGroup()
        self.y_cursors_single_dual_radio_button_group.addButton(self.y_cursors_single_radio_button)
        self.y_cursors_single_dual_radio_button_group.addButton(self.y_cursors_dual_radio_button)

        self.t_cursors_enabled_check_box = QtWidgets.QCheckBox('T Cursors')
        self.t_cursors_single_radio_button = QtWidgets.QRadioButton('Single')
        self.t_cursors_dual_radio_button = QtWidgets.QRadioButton('Dual')
        self.t_cursors_dual_radio_button.setChecked(True)
        self.t_cursors_single_dual_radio_button_group = QtWidgets.QButtonGroup()
        self.t_cursors_single_dual_radio_button_group.addButton(self.t_cursors_single_radio_button)
        self.t_cursors_single_dual_radio_button_group.addButton(self.t_cursors_dual_radio_button)

        self.layout = QtWidgets.QGridLayout()
        self.layout.addWidget(self.y_cursors_enabled_check_box, 0, 0)
        self.layout.addWidget(self.y_cursors_single_radio_button, 0, 1)
        self.layout.addWidget(self.y_cursors_dual_radio_button, 0, 2)
        self.layout.addWidget(self.t_cursors_enabled_check_box, 1, 0)
        self.layout.addWidget(self.t_cursors_single_radio_button, 1, 1)
        self.layout.addWidget(self.t_cursors_dual_radio_button, 1, 2)
        self.setLayout(self.layout)


class ChannelsWidget(QtWidgets.QWidget):
    def __init__(self):
        super().__init__()

        self.find_push_button = QtWidgets.QPushButton('Find')

        self.add_channel_line_edit = QtWidgets.QLineEdit()
        self.add_channel_line_edit.setAlignment(QtCore.Qt.AlignRight)
        self.add_channel_push_button = QtWidgets.QPushButton('Add')

        self.remove_channel_combo_box = QtWidgets.QComboBox()
        self.remove_channel_push_button = QtWidgets.QPushButton('Remove')

        self.layout = QtWidgets.QGridLayout()
        self.layout.addWidget(self.find_push_button, 0, 1)
        self.layout.addWidget(self.add_channel_line_edit, 1, 0)
        self.layout.addWidget(self.add_channel_push_button, 1, 1)
        self.layout.addWidget(self.remove_channel_combo_box, 2, 0)
        self.layout.addWidget(self.remove_channel_push_button, 2, 1)
        self.layout.setColumnMinimumWidth(0, 250)
        self.setLayout(self.layout)


class PlotMenu(QtWidgets.QMenu):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.view_all_data_action = QtWidgets.QAction('View All Data')
        self.addAction(self.view_all_data_action)

        self.reset_t0_to_point_action = QtWidgets.QAction('Reset T0 To Point')
        self.addAction(self.reset_t0_to_point_action)

        self.mouse_mode_sub_menu = self.addMenu('Mouse Mode')
        self.mouse_mode_widget = MouseModeWidget()
        self.mouse_mode_action = QtWidgets.QWidgetAction(self)
        self.mouse_mode_action.setDefaultWidget(self.mouse_mode_widget)
        self.mouse_mode_sub_menu.addAction(self.mouse_mode_action)

        self.y_scale_sub_menu = self.addMenu('Y Scale')
        self.y_scale_widget = ScaleWidget()
        self.y_scale_action = QtWidgets.QWidgetAction(self)
        self.y_scale_action.setDefaultWidget(self.y_scale_widget)
        self.y_scale_sub_menu.addAction(self.y_scale_action)

        self.cursors_sub_menu = self.addMenu('Cursors')
        self.cursors_widget = CursorsWidget()
        self.cursors_action = QtWidgets.QWidgetAction(self)
        self.cursors_action.setDefaultWidget(self.cursors_widget)
        self.cursors_sub_menu.addAction(self.cursors_action)

        self.channels_sub_menu = self.addMenu('Channels')
        self.channels_widget = ChannelsWidget()
        self.channels_action = QtWidgets.QWidgetAction(self)
        self.channels_action.setDefaultWidget(self.channels_widget)
        self.channels_sub_menu.addAction(self.channels_action)

        self.plots_sub_menu = self.addMenu('Plots')
        self.plots_add_to_row_action = QtWidgets.QAction('Add To Row')
        self.plots_sub_menu.addAction(self.plots_add_to_row_action)
        self.plots_add_to_col_action = QtWidgets.QAction('Add To Column')
        self.plots_sub_menu.addAction(self.plots_add_to_col_action)
        self.plots_remove_action = QtWidgets.QAction('Remove')
        self.plots_sub_menu.addAction(self.plots_remove_action)
