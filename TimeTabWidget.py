from qtpy import QtWidgets


class TimeTabWidget(QtWidgets.QWidget):
    def __init__(self):
        super().__init__()

        self.relative_time_radio_button = QtWidgets.QRadioButton('Relative')
        self.relative_time_radio_button.setChecked(True)
        self.gps_time_radio_button = QtWidgets.QRadioButton('GPS')
        self.utc_time_radio_button = QtWidgets.QRadioButton('UTC')
        self.local_time_radio_button = QtWidgets.QRadioButton('Local')

        self.layout = QtWidgets.QHBoxLayout()
        self.layout.addWidget(self.relative_time_radio_button)
        self.layout.addWidget(self.gps_time_radio_button)
        self.layout.addWidget(self.utc_time_radio_button)
        self.layout.addWidget(self.local_time_radio_button)
        self.layout.setSizeConstraint(QtWidgets.QLayout.SetFixedSize)

        self.setLayout(self.layout)
