import numpy as np
from qtpy import QtCore, QtGui
import pyqtgraph as pg

import PlotMenu
import Transforms
import Cursors


max_lin = np.finfo(float).max
min_lin = np.finfo(float).min
max_log = np.log10(max_lin) - 1
min_log = -np.log10(abs(min_lin))


class Plot(pg.PlotItem):
    log_mode_toggled = QtCore.Signal(QtCore.QObject, bool)

    def __init__(self, scope, row, col):
        super().__init__(axisItems={'bottom': pg.AxisItem('bottom')})

        self.scope = scope
        self.row = row
        self.col = col

        self.menu = PlotMenu.PlotMenu()
        self.ctrlMenu = None
        self.getViewBox().menu = self.menu

        # If all the plots are removed then there is no means to access a plot context menu to add a plot.
        # Therefore, the option to remove the plot at (0, 0) is disabled.
        if row == 0 and col == 0:
            self.menu.plots_remove_action.setDisabled(True)

        self.transform = Transforms.IdTransform()

        # y cursors

        self.y_single_cursor = Cursors.SingleCursor(False, pg.mkPen(style=QtCore.Qt.DashLine))
        self.y_single_cursor.set_point_to_label_function(lambda pt: 'Y={:g}'.format(pt.y()))
        self.y_single_cursor.set_plot(self)
        self.y_single_cursor.set_transform(self.transform)
        self.y_single_cursor.set_val(QtCore.QPointF(0, 0))
        self.y_single_cursor.set_enabled(False)

        self.y_dual_cursors = Cursors.DualCursors(False, pg.mkPen(style=QtCore.Qt.DashLine))
        self.y_dual_cursors.set_cursor_1_point_to_label_function(lambda pt: 'Y1={:g}'.format(pt.y()))
        self.y_dual_cursors.set_cursor_2_point_to_label_function(lambda pt: 'Y2={:g}'.format(pt.y()))
        self.y_dual_cursors.set_diff_point_pair_to_label_function(lambda pt1, pt2: 'ΔY={:g}'.format(abs(pt2.y() - pt1.y())))
        self.y_dual_cursors.set_plot(self)
        self.y_dual_cursors.set_transform(self.transform)
        self.y_dual_cursors.set_cursor_1_val(QtCore.QPointF(0, -0.5))
        self.y_dual_cursors.set_cursor_2_val(QtCore.QPointF(0, 0.5))
        self.y_dual_cursors.set_enabled(False)

        # t cursors

        self.t_single_cursor = Cursors.SingleCursor(True, pg.mkPen(style=QtCore.Qt.DashLine))
        self.t_single_cursor.set_point_to_label_function(lambda pt: 'T={:g}'.format(pt.x()))
        self.t_single_cursor.set_plot(self)
        self.t_single_cursor.set_transform(self.transform)
        self.t_single_cursor.set_val(QtCore.QPointF(0, 0))
        self.t_single_cursor.set_enabled(False)

        self.t_dual_cursors = Cursors.DualCursors(True, pg.mkPen(style=QtCore.Qt.DashLine))
        self.t_dual_cursors.set_cursor_1_point_to_label_function(lambda pt: 'T1={:g}'.format(pt.x()))
        self.t_dual_cursors.set_cursor_2_point_to_label_function(lambda pt: 'T2={:g}'.format(pt.x()))
        self.t_dual_cursors.set_diff_point_pair_to_label_function(lambda pt1, pt2: 'ΔT={:g}'.format(abs(pt2.x() - pt1.x())))
        self.t_dual_cursors.set_plot(self)
        self.t_dual_cursors.set_transform(self.transform)
        self.t_dual_cursors.set_cursor_1_val(QtCore.QPointF(-0.5, 0))
        self.t_dual_cursors.set_cursor_2_val(QtCore.QPointF(0.5, 0))
        self.t_dual_cursors.set_enabled(False)

        # y scale

        [[_, _], [y_min, y_max]] = self.getViewBox().viewRange()
        self.menu.y_scale_widget.manual_line_edit_1.setText(str(y_min))
        self.menu.y_scale_widget.manual_line_edit_2.setText(str(y_max))

        # signals

        # mouse mode
        self.menu.mouse_mode_widget.pan_zoom_radio_button.toggled.connect(self.mouse_mode_pan_zoom_radio_button_toggled_slot)
        self.menu.mouse_mode_widget.zoom_box_radio_button.toggled.connect(self.mouse_mode_zoom_box_radio_button_toggled_slot)

        # cursors

        self.menu.cursors_widget.y_cursors_enabled_check_box.toggled.connect(lambda checked: self.update_y_cursors())
        self.menu.cursors_widget.y_cursors_single_radio_button.toggled.connect(lambda checked: self.update_y_cursors())
        self.menu.cursors_widget.y_cursors_dual_radio_button.toggled.connect(lambda checked: self.update_y_cursors())

        self.menu.cursors_widget.t_cursors_enabled_check_box.toggled.connect(lambda checked: self.update_t_cursors())
        self.menu.cursors_widget.t_cursors_single_radio_button.toggled.connect(lambda checked: self.update_t_cursors())
        self.menu.cursors_widget.t_cursors_dual_radio_button.toggled.connect(lambda checked: self.update_t_cursors())

        # y scale

        self.menu.y_scale_widget.manual_radio_button.clicked.connect(self.y_scale_manual_radio_button_clicked_slot)
        self.menu.y_scale_widget.manual_line_edit_1.editingFinished.connect(self.y_scale_manual_line_edit_1_editing_finished_slot)
        self.menu.y_scale_widget.manual_line_edit_2.editingFinished.connect(self.y_scale_manual_line_edit_2_editing_finished_slot)

        self.menu.y_scale_widget.auto_radio_button.toggled.connect(self.y_scale_auto_radio_button_toggled_slot)
        self.menu.y_scale_widget.auto_percent_spin_box.valueChanged.connect(self.y_scale_auto_percent_spin_box_value_changed_slot)

        self.menu.y_scale_widget.log_mode_check_box.clicked.connect(self.log_mode_check_box_clicked_slot)

        # plots

        self.menu.plots_add_to_row_action.triggered.connect(lambda: self.scope.add_plot_to_row(self.row, self.col))
        self.menu.plots_add_to_col_action.triggered.connect(lambda: self.scope.add_plot_to_col(self.row, self.col))
        self.menu.plots_remove_action.triggered.connect(lambda: self.scope.remove_plot(self.row, self.col))

    # slots

    # mouse mode

    def mouse_mode_pan_zoom_radio_button_toggled_slot(self, checked):
        if checked:
            self.getViewBox().setMouseMode(pg.ViewBox.PanMode)

    def mouse_mode_zoom_box_radio_button_toggled_slot(self, checked):
        if checked:
            self.getViewBox().setMouseMode(pg.ViewBox.RectMode)

    # cursors

    def update_y_cursors(self):
        if self.menu.cursors_widget.y_cursors_enabled_check_box.isChecked():
            if self.menu.cursors_widget.y_cursors_single_radio_button.isChecked():
                self.y_single_cursor.set_enabled(True)
                self.y_dual_cursors.set_enabled(False)
            else:
                self.y_single_cursor.set_enabled(False)
                self.y_dual_cursors.set_enabled(True)
        else:
            self.y_single_cursor.set_enabled(False)
            self.y_dual_cursors.set_enabled(False)

    def update_t_cursors(self):
        if self.menu.cursors_widget.t_cursors_enabled_check_box.isChecked():
            if self.menu.cursors_widget.t_cursors_single_radio_button.isChecked():
                self.t_single_cursor.set_enabled(True)
                self.t_dual_cursors.set_enabled(False)

            else:
                self.t_single_cursor.set_enabled(False)
                self.t_dual_cursors.set_enabled(True)
        else:
            self.t_single_cursor.set_enabled(False)
            self.t_dual_cursors.set_enabled(False)

    # y scale

    def y_scale_manual_radio_button_clicked_slot(self, checked):
        if checked:
            self.set_y_scale_manual_range()

    def y_scale_manual_line_edit_1_editing_finished_slot(self):
        self.menu.y_scale_widget.manual_radio_button.setChecked(True)
        self.set_y_scale_manual_range()

    def y_scale_manual_line_edit_2_editing_finished_slot(self):
        self.menu.y_scale_widget.manual_radio_button.setChecked(True)
        self.set_y_scale_manual_range()

    def set_y_scale_manual_range(self):
        if self.menu.y_scale_widget.manual_line_edit_1.text() and self.menu.y_scale_widget.manual_line_edit_2.text():
            y1_val = float(self.menu.y_scale_widget.manual_line_edit_1.text())
            y2_val = float(self.menu.y_scale_widget.manual_line_edit_2.text())
            if y1_val is not None and y2_val is not None:
                if y1_val < y2_val:
                    y_min = y1_val
                    y_max = y2_val
                else:
                    y_min = y2_val
                    y_max = y1_val

                y_min = self.transform.y_transform(y_min)
                y_max = self.transform.y_transform(y_max)
                if np.isfinite(y_min) and np.isfinite(y_max):
                    self.getViewBox().setRange(yRange=(y_min, y_max), padding=None, update=True, disableAutoRange=True)

    def y_scale_auto_radio_button_toggled_slot(self, checked):
        if checked:
            percent = self.menu.y_scale_widget.auto_percent_spin_box.value()
            self.set_y_scale_auto_percent(percent)

    def y_scale_auto_percent_spin_box_value_changed_slot(self, i):
        self.menu.y_scale_widget.auto_radio_button.setChecked(True)
        self.set_y_scale_auto_percent(i)

    def set_y_scale_auto_percent(self, percent):
        self.getViewBox().enableAutoRange(axis=pg.ViewBox.YAxis, enable=percent)

    def log_mode_check_box_clicked_slot(self, checked):
        if checked:
            self.vb.setLimits(yMin=min_log, yMax=max_log, minYRange=min_log, maxYRange=max_log)
            [[_, _], [y_min, y_max]] = self.getViewBox().viewRange()
            if y_min < min_log and y_max > max_log:
                self.vb.setYRange(min_log, max_log)
            elif y_min < min_log:
                self.vb.setYRange(min_log, y_max)
            elif y_max > max_log:
                self.vb.setYRange(y_min, max_log)

            self.setLogMode(False, True)
            self.transform = Transforms.LogYTransform()
        else:
            self.vb.setLimits(yMin=min_lin, yMax=max_lin, minYRange=min_lin, maxYRange=max_lin)
            self.setLogMode(False, False)
            self.transform = Transforms.IdTransform()

        self.y_single_cursor.set_transform(self.transform)
        self.y_dual_cursors.cursor_1.set_transform(self.transform)
        self.y_dual_cursors.cursor_2.set_transform(self.transform)
        self.log_mode_toggled.emit(self, checked)

    # style

    def set_foreground_color(self, color):
        self.y_single_cursor.set_foreground_color(color)
        self.y_dual_cursors.set_foreground_color(color)

        self.t_single_cursor.set_foreground_color(color)
        self.t_dual_cursors.set_foreground_color(color)

        self.getAxis('left').setPen(QtGui.QPen(color))
        self.getAxis('left').setTextPen(QtGui.QPen(color))

        self.getAxis('bottom').setPen(QtGui.QPen(color))
        self.getAxis('bottom').setTextPen(QtGui.QPen(color))

    def set_font(self, font):
        self.t_single_cursor.set_font(font)
        self.t_dual_cursors.set_font(font)

        self.y_single_cursor.set_font(font)
        self.y_dual_cursors.set_font(font)

        self.getAxis('left').setTickFont(font)
        self.getAxis('left').label.setFont(font)

        self.getAxis('bottom').setTickFont(font)
        self.getAxis('bottom').label.setFont(font)
