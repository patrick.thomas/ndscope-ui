from qtpy import QtWidgets


class StyleTabWidget(QtWidgets.QWidget):
    def __init__(self):
        super().__init__()

        self.font_size_spin_box_label = QtWidgets.QLabel('Font Size:')
        self.font_size_spin_box = QtWidgets.QSpinBox()
        self.font_size_spin_box.setMinimum(1)

        self.color_mode_label = QtWidgets.QLabel('Color Mode:')
        self.color_mode_dark_radio_button = QtWidgets.QRadioButton('Dark')
        self.color_mode_dark_radio_button.setChecked(True)
        self.color_mode_light_radio_button = QtWidgets.QRadioButton('Light')

        self.font_size_layout = QtWidgets.QHBoxLayout()
        self.font_size_layout.addWidget(self.font_size_spin_box_label)
        self.font_size_layout.addWidget(self.font_size_spin_box)

        self.color_mode_layout = QtWidgets.QHBoxLayout()
        self.color_mode_layout.addWidget(self.color_mode_label)
        self.color_mode_layout.addWidget(self.color_mode_dark_radio_button)
        self.color_mode_layout.addWidget(self.color_mode_light_radio_button)

        self.layout = QtWidgets.QVBoxLayout()
        self.layout.addLayout(self.font_size_layout)
        self.layout.addLayout(self.color_mode_layout)
        self.layout.setSizeConstraint(QtWidgets.QLayout.SetFixedSize)

        self.setLayout(self.layout)
